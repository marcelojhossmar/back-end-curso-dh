/**
 * @author: Edson A. Terceros T.
 */

package com.example.aplication_dia2.controller;

import org.springframework.stereotype.Controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Controller
public class IndexController {
    @GET
    @Path("/{\"\" | / | /index}")
    public String getIndexPage() {
        return "index";
    }
}
