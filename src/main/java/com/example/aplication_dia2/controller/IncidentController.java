/**
 * @author: Edson A. Terceros T.
 */

package com.example.aplication_dia2.controller;

import com.example.aplication_dia2.dao.IncidentCommand;
import com.example.aplication_dia2.model.Incident;
import com.example.aplication_dia2.services.IncidentService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/incident")
@Produces("application/json")
@CrossOrigin
public class IncidentController {
    private IncidentService service;


    public IncidentController(IncidentService service) {
        this.service = service;

    }

    @GET
    public Response getItems() {
        List<IncidentCommand> items = new ArrayList<>();
        service.findAll().forEach(item -> {
            IncidentCommand itemCommand = new IncidentCommand(item);
            items.add(itemCommand);
        });
        return Response.ok(items).build();
    }

    @GET
    @Path("/{id}")
    public Response getItemsById(@PathParam("id") @NotNull Long id) {
        Incident item = service.findById(id);
        return Response.ok(new IncidentCommand(item)).build();
    }

    @POST
    public Response saveItem(IncidentCommand item) {
        Incident model = item.toDomain();
      //  model.setSubCategory(subCategoryService.findById(item.getSubCategoryId()));
        Incident itemPersisted = service.save(model);
        return Response.ok(new IncidentCommand(itemPersisted)).build();
    }

    @PUT
    public Response updateItem(Incident item) {
        Incident itemPersisted = service.save(item);
        return Response.ok(new IncidentCommand(itemPersisted)).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteItem(@PathParam("id") String id) {
        service.deleteById(Long.valueOf(id));
        return Response.ok().build();
    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }

}