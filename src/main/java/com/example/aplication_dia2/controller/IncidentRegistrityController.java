/**
 * @author: Edson A. Terceros T.
 */

package com.example.aplication_dia2.controller;

import com.example.aplication_dia2.dao.IncidentRegistryCommand;
import com.example.aplication_dia2.model.IncidentRegistry;
import com.example.aplication_dia2.services.IncidentRegistryService;
import com.example.aplication_dia2.services.IncidentService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/incidentregistrity")
@Produces("application/json")
@CrossOrigin
public class IncidentRegistrityController {
    private IncidentRegistryService service;
    private IncidentService incidentService;


    public IncidentRegistrityController(IncidentRegistryService service, IncidentService incidentService) {
        this.service = service;

        this.incidentService = incidentService;
    }


    @GET
    public Response getItems() {
        List<IncidentRegistryCommand> items = new ArrayList<>();
        service.findAll().forEach(item -> {
            IncidentRegistryCommand itemCommand = new IncidentRegistryCommand(item);
            items.add(itemCommand);
        });
        return Response.ok(items).build();
    }

    @GET
    @Path("/{id}")
    public Response getItemsById(@PathParam("id") @NotNull Long id) {
        IncidentRegistry item = service.findById(id);
        return Response.ok(new IncidentRegistryCommand(item)).build();
    }

    @POST
    public Response saveItem(IncidentRegistryCommand item) {
        IncidentRegistry model = item.toDomain();
        model.setIncident(incidentService.findById(item.getIncidentId()));
        IncidentRegistry itemPersisted = service.save(model);
        return Response.ok(new IncidentRegistryCommand(itemPersisted)).build();
    }

    @PUT
    public Response updateItem(IncidentRegistry item) {
        IncidentRegistry itemPersisted = service.save(item);
        return Response.ok(new IncidentRegistryCommand(itemPersisted)).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteItem(@PathParam("id") String id) {
        service.deleteById(Long.valueOf(id));
        return Response.ok().build();
    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }

}