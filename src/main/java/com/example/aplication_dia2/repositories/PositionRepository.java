package com.example.aplication_dia2.repositories;

import com.example.aplication_dia2.model.Position;
import org.springframework.data.repository.CrudRepository;

public interface PositionRepository extends CrudRepository<Position, Long> {
}
