package com.example.aplication_dia2.repositories;

import com.example.aplication_dia2.model.Incident;
import org.springframework.data.repository.CrudRepository;

public interface IncidentRepository extends CrudRepository<Incident, Long> {
}