package com.example.aplication_dia2.repositories;

import com.example.aplication_dia2.model.Item;
import org.springframework.data.repository.CrudRepository;

public interface ItemRepository extends CrudRepository<Item, Long> {
}
