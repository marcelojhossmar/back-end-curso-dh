package com.example.aplication_dia2.repositories;

import com.example.aplication_dia2.model.Category;
import com.example.aplication_dia2.model.SubCategory;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface SubCategoryRepository extends CrudRepository<SubCategory, Long> {
    Optional<List<Category>> findByCode(String code);
}
