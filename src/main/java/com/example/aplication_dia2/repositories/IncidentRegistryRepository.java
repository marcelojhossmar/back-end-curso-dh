package com.example.aplication_dia2.repositories;

import com.example.aplication_dia2.model.IncidentRegistry;
import org.springframework.data.repository.CrudRepository;

public interface IncidentRegistryRepository extends CrudRepository<IncidentRegistry, Long> {
}