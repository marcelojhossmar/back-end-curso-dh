package com.example.aplication_dia2.repositories;

import com.example.aplication_dia2.model.Contract;
import org.springframework.data.repository.CrudRepository;

public interface ContractRepository extends CrudRepository<Contract, Long> {
}
