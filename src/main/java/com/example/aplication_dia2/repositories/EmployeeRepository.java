/**
 * @author: Edson A. Terceros T.
 */

package com.example.aplication_dia2.repositories;

import com.example.aplication_dia2.model.Employee;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {
    Optional<List<Employee>> findByFirstName(String firstName);
}
