package com.example.aplication_dia2;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class lifeCicleSpring implements InitializingBean,
                                        DisposableBean,
                                        BeanNameAware,
                                        BeanFactoryAware,
                                        ApplicationContextAware {


    public lifeCicleSpring() {
        System.out.println("Constructor:" + "lifeCicleSpring");
    }

    @PostConstruct //  luego de que se contruya el been se ejecutara esto
    public void postConstruct(){
        System.out.println("postConstruct");

    }

    @PreDestroy //  luego de que se contruya el been se ejecutara esto
    public void preDestroy(){
        System.out.println("preDestroy");
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        System.out.println("setBeanFactory");
    }

    @Override
    public void setBeanName(String s) {
        System.out.println("setBeanName");

    }

    @Override
    public void destroy() throws Exception {
        System.out.println("destroy");

    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("afterPropertiesSet");

    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {

        System.out.println("setApplicationContext");
    }

    public void beforeInit(){
        System.out.println("beforeInit");
    }
    public void afterInit(){
        System.out.println("beforeInit");
    }

}
