package com.example.aplication_dia2;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

@Component
public class MyBeenPostPRocessor implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof lifeCicleSpring){
            ((lifeCicleSpring) bean).beforeInit();
            System.out.println("nombre del Bean: "+beanName);
        }

        return bean;
    }



    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof lifeCicleSpring) {
            ((lifeCicleSpring) bean).afterInit();
        }
        return bean;

    }
}
