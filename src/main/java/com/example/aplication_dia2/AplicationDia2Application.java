package com.example.aplication_dia2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AplicationDia2Application {

    public static void main(String[] args) {
        SpringApplication.run(AplicationDia2Application.class, args);
    }
}
