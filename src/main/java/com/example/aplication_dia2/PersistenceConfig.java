package com.example.aplication_dia2;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/*
* Esta clase es para ls campos de auditoria
* En esta clase se ponen las configuration
* */
@Configuration // Componente de tipo configuracion
@EnableJpaAuditing // ya que estamos utilizando columnas de auditoria
public class PersistenceConfig {
}
