/**
 * @author: Edson A. Terceros T.
 */

package com.example.aplication_dia2.model;

import javax.persistence.Entity;

@Entity
public class Incident extends ModelBase {

    int codi;

    String nombre;


    public int getCodi() {
        return codi;
    }

    public void setCodi(int codi) {
        this.codi = codi;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


  }
