/**
 * @author: Edson A. Terceros T.
 */

package com.example.aplication_dia2.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.util.Date;

@Entity
public class IncidentRegistry extends ModelBase {


    Date fecha_registro;

    @OneToOne(optional = true)
    Incident incident;

    public Date getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(Date fecha_registro) {
        this.fecha_registro = fecha_registro;
    }

    public Incident getIncident() {
        return incident;
    }

    public void setIncident(Incident incident) {
        this.incident = incident;
    }
}
