/**
 * @author: Edson A. Terceros T.
 */

package com.example.aplication_dia2.services;

import com.example.aplication_dia2.model.Category;

import java.util.List;


public interface CategoryService extends GenericService<Category> {
    List<Category> findByCode(String code);
}
