package com.example.aplication_dia2.services;

import com.example.aplication_dia2.model.Incident;

public interface IncidentService extends GenericService<Incident> {
}