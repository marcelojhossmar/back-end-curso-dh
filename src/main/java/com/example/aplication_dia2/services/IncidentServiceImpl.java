/**
 * @author: Edson A. Terceros T.
 */

package com.example.aplication_dia2.services;

import com.example.aplication_dia2.model.Incident;
import com.example.aplication_dia2.repositories.IncidentRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class IncidentServiceImpl extends GenericServiceImpl<Incident> implements IncidentService {
    private IncidentRepository repositories;

    public IncidentServiceImpl(IncidentRepository repositories) {
        this.repositories = repositories;
    }

    @Override
    protected CrudRepository<Incident, Long> getRepository() {
        return repositories;
    }
}