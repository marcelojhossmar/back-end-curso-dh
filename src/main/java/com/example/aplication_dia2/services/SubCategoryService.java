/**
 * @author: Edson A. Terceros T.
 */

package com.example.aplication_dia2.services;

import com.example.aplication_dia2.model.SubCategory;

public interface SubCategoryService extends GenericService<SubCategory> {
}