/**
 * @author: Edson A. Terceros T.
 */

package com.example.aplication_dia2.services;

import java.util.List;


public interface GenericService<T> {
    List<T> findAll();

    T findById(Long id);

    T save(T model);

    void deleteById(Long id);
}
