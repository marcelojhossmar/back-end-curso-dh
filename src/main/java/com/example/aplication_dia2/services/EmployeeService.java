/**
 * @author: Edson A. Terceros T.
 */

package com.example.aplication_dia2.services;

import com.example.aplication_dia2.model.Employee;
import org.springframework.stereotype.Service;

import java.io.InputStream;

@Service
public interface EmployeeService extends GenericService<Employee> {
    void saveImage(Long id, InputStream inputStream);
}
