/**
 * @author: Edson A. Terceros T.
 */

package com.example.aplication_dia2.services;

import com.example.aplication_dia2.model.Item;

import java.io.InputStream;

public interface ItemService extends GenericService<Item> {
    void saveImage(Long id, InputStream file);
}