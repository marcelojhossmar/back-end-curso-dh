package com.example.aplication_dia2.services;

import com.example.aplication_dia2.model.IncidentRegistry;

public interface IncidentRegistryService extends GenericService<IncidentRegistry> {
}