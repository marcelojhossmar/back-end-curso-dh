/**
 * @author: Edson A. Terceros T.
 */

package com.example.aplication_dia2.services;

import com.example.aplication_dia2.model.IncidentRegistry;
import com.example.aplication_dia2.repositories.IncidentRegistryRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class IncidentRegistryServiceImpl extends GenericServiceImpl<IncidentRegistry> implements IncidentRegistryService {
    private IncidentRegistryRepository repositories;

    public IncidentRegistryServiceImpl(IncidentRegistryRepository repositories) {
        this.repositories = repositories;
    }

    @Override
    protected CrudRepository<IncidentRegistry, Long> getRepository() {
        return repositories;
    }
}