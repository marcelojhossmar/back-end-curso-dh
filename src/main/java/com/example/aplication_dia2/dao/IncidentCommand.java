/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.example.aplication_dia2.dao;


import com.example.aplication_dia2.model.Incident;
import com.example.aplication_dia2.model.ModelBase;

public class IncidentCommand extends ModelBase {

    int codi;
    String nombre;

    public IncidentCommand() {

    }

    public IncidentCommand(Incident incident) {
        this.setCodi(incident.getCodi());
        this.setNombre(incident.getNombre());

    }

    public int getCodi() {
        return codi;
    }

    public void setCodi(int codi) {
        this.codi = codi;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Incident toDomain() {
        Incident incident = new Incident();
        incident.setCodi(getCodi());
        incident.setNombre(getNombre());

        return incident;
    }
    ;


}
