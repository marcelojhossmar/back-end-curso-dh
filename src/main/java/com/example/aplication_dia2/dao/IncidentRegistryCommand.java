/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.example.aplication_dia2.dao;


import com.example.aplication_dia2.model.Incident;
import com.example.aplication_dia2.model.IncidentRegistry;
import com.example.aplication_dia2.model.ModelBase;

import java.util.Date;

public class IncidentRegistryCommand extends ModelBase {

    Date fecha_registro;
    Incident incident;

    public IncidentRegistryCommand() {

    }

    public IncidentRegistryCommand(IncidentRegistry incident) {
        this.fecha_registro = incident.getFecha_registro();
        this.incident = incident.getIncident();
    }

    public Date getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(Date fecha_registro) {
        this.fecha_registro = fecha_registro;
    }

    public Incident getIncident() {
        return incident;
    }

    public void setIncident(Incident incident) {
        this.incident = incident;
    }

    public IncidentRegistry toDomain() {
        IncidentRegistry incident = new IncidentRegistry();
        incident.setFecha_registro(getFecha_registro());
        incident.setIncident(getIncident());
        return incident;
    }

    public Long getIncidentId() {
        return incident.getId();
    }

}
